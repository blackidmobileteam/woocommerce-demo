package com.example.woocommercedemo.utils;

import java.io.Serializable;

public class ProductAttributes implements Serializable{
	
	private static final long serialVersionUID = 551391775743243854L;
	String attributeName;
	int position;
	boolean isVisible;
	boolean variation;
	String[] options;
	
	public ProductAttributes(){
		
	}
	
	public ProductAttributes(String name,int position,boolean isVisible,boolean variation,String[] options){
		attributeName	= name;
		this.position	= position;
		this.isVisible	= isVisible;
		this.variation	= variation;
		this.options	= options;
		
	}
	
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public boolean isVisible() {
		return isVisible;
	}
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	public boolean isVariation() {
		return variation;
	}
	public void setVariation(boolean variation) {
		this.variation = variation;
	}
	public String[] getOptions() {
		return options;
	}
	public void setOptions(String[] options) {
		this.options = options;
	}
	
}
