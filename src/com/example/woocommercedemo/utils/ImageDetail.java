package com.example.woocommercedemo.utils;

import java.io.Serializable;

public class ImageDetail implements Serializable{
	
	private static final long serialVersionUID = 4530161921291044301L;
	int imageServerId;
	String imgSrc;
	String imageTitle;
	int position;
	
	public ImageDetail() {
		// TODO Auto-generated constructor stub
		imageServerId	= 0;
		imgSrc			= "";
		imageTitle		= "";
		position		= 0;
	}
	
	public ImageDetail(int id,String src,String title,int position){
		imageServerId	= id;
		imgSrc			= src;
		imageTitle		= title;
		this.position	= position;
	}
	
	public void setImageServerId(int id){
		imageServerId	= id;
	}
	
	public void setImgSrc(String src){
		imgSrc			= src;
	}
	
	public void setImageTitle(String title){
		imageTitle		= title;
	}
	
	public void setPosition(int position){
		this.position	= position;
	}
	
	public int getImageServerId(){
		return imageServerId;
	}
	
	public String getImgSrc(){
		return imgSrc;
	}
	
	public String getImageTitle(){
		return imageTitle;
	}
	
	public int getPosition(){
		return position;
	}
}
