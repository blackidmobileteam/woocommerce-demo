package com.example.woocommercedemo.utils;

import java.io.Serializable;

public class ProductDimensions implements Serializable{
	
	private static final long serialVersionUID = 8637235031216650964L;
	float productLength;
	float productWidth;
	float productHeight;
	String unit;
	
	public ProductDimensions(){
		
	}
	
	public ProductDimensions(float l,float w,float h,String unit){
		productLength	= l;
		productWidth	= w;
		productHeight	= h;
		this.unit		= unit;
	}
	
	public void setProductLength(float l){
		productLength	= l;
	}
	
	public void setProductWidth(float w){
		productWidth	= w;
	}
	
	public void setProductHeight(float h){
		productHeight	= h;
	}
	
	public void setUnit(String unit){
		this.unit		= unit;
	}
	
	public float getProductLength(){
		return productLength;
	}
	
	public float getProductWidth(){
		return productWidth;
	}
	
	public float getProductHeight(){
		return productHeight;
	}
	
	public String getUnit(){
		return unit;
	}
}
