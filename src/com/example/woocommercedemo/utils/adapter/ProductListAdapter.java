package com.example.woocommercedemo.utils.adapter;

import java.util.ArrayList;

import com.example.woocommercedemo.R;
import com.example.woocommercedemo.utils.Product;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProductListAdapter extends BaseAdapter{
	
	Context context;
	ArrayList<Product>  productData;
	SparseArray<Bitmap> produtImgData;
	LayoutInflater inflater;
	TextView productTitle;
	ImageView productImage;
	
	public ProductListAdapter(Context con,ArrayList<Product> products) {
		// TODO Auto-generated constructor stub
		context			= con;
		productData		= products;
		inflater		= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		produtImgData	= new SparseArray<Bitmap>();
	}
	
	public void setProductImgData(SparseArray<Bitmap> productFetureImgs){
		produtImgData	= productFetureImgs;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return productData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView	= inflater.inflate(R.layout.product_list_item, parent, false);
		
		productTitle	= (TextView)convertView.findViewById(R.id.product_name);
		productImage	= (ImageView)convertView.findViewById(R.id.product_img);
		
		productTitle.setText(productData.get(position).getProductTtile());
		
		int id			= productData.get(position).getProductServerId();
		
		if(produtImgData.get(id) != null){
			productImage.setImageBitmap(produtImgData.get(id));
		}
		
		return convertView;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}

}
