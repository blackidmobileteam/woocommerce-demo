package com.example.woocommercedemo.utils.adapter;

import java.util.ArrayList;

import com.example.woocommercedemo.R;
import com.example.woocommercedemo.utils.ProductAttributes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

public class VariationSpinnerAdapter extends ArrayAdapter<ProductAttributes>{

	ArrayList<ProductAttributes> variations;
	Context context;
	
	public VariationSpinnerAdapter(Context con, int textViewResourceId, ArrayList<ProductAttributes> objects) {
		// TODO Auto-generated constructor stub
		super(con, textViewResourceId,objects);
		
		variations	= objects;
		context		= con;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomView(position, convertView, parent);
	}

	@Override
	public ProductAttributes getItem(int position) {
		// TODO Auto-generated method stub
		return variations.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return super.getItemId(position);
	}
	
	public View getCustomView(int position, View convertView, ViewGroup parent){
		
		LayoutInflater inflater	= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		convertView		= inflater.inflate(R.layout.spinner_selected_item, parent, false);
		
		TextView spinnerSelectedText = (TextView)convertView.findViewById(R.id.spinner_selected_title);
		
		spinnerSelectedText.setText(variations.get(position).getAttributeName());
		return convertView;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomDropDownView(position, convertView, parent);
	}

	
	public View getCustomDropDownView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater	= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		convertView			= inflater.inflate(R.layout.spinner_dropdown_item, parent, false);
		
		CheckedTextView	txt	= (CheckedTextView)convertView.findViewById(R.id.dropdown_txt);
		
		txt.setText(variations.get(position).getAttributeName());
		
		return convertView;
		
	}
	
}
