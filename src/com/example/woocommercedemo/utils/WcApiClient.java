package com.example.woocommercedemo.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

public class WcApiClient {
	
	final static String API_ENDPOINT 	= "wc-api/v1/";
	final static String HASH_ALGORITHM	= "SHA256";
	
	private String apiUrl;
	private String consumerKey;
	private String consumerSecret;
	private boolean isSsl = false;
	String params="";
	URL url1;
	String mainUrl;
	HttpURLConnection urlConnection;
	ProgressDialog dialog;
	Context context;
	String filePath;
	File dir;
	
	public WcApiClient(String consumerKey,String consumerSecret,String url,Context con,boolean isSSL) throws Exception {
		// TODO Auto-generated constructor stub
		if((!consumerKey.equalsIgnoreCase("") && consumerKey != null) &&
			(!consumerSecret.equalsIgnoreCase("") && consumerSecret != null) &&
			(!url.equalsIgnoreCase("") && url != null)){
			
			this.apiUrl 		= url+API_ENDPOINT;
			this.consumerKey	= consumerKey;
			this.consumerSecret	= consumerSecret;
			this.isSsl			= isSSL;
			context				= con;
			dialog				= new ProgressDialog(context);
			dialog.setCancelable(true);
			dialog.setMessage("Loading Data..........");
			dialog.setCanceledOnTouchOutside(false);
			
		}else if((consumerKey.equalsIgnoreCase("") || consumerKey==null) && 
				(consumerSecret.equalsIgnoreCase("") || consumerSecret==null)){
			throw new Exception("Error : Consumer Key / Consumer Secret missing.");
		}else{
			throw new Exception("Error : Store URL missing.");
		}
		initialization();
	}
	
	public WcApiClient(String consumerKey,String consumerSecret,String url,Context con) {
		// TODO Auto-generated constructor stub
		this.apiUrl 		= url+API_ENDPOINT;
		this.consumerKey	= consumerKey;
		this.consumerSecret	= consumerSecret;
		context				= con;
		dialog				= new ProgressDialog(context);
		
		dialog.setCancelable(true);
		dialog.setMessage("Loading Data..........");
		dialog.setCanceledOnTouchOutside(false);
		initialization();
	}
	
	public void initialization(){
		filePath 		= Environment.getExternalStorageDirectory().getAbsolutePath() + "/woocommerce";
		dir				= new File(filePath);
        if(!dir.exists()){
        	dir.mkdirs();
        }
	}
	
	public void setConsumerKey(String consumerKey){
		this.consumerKey = consumerKey;
	}
	
	public void setConsumerSecret(String consumerSecret){
		this.consumerSecret = consumerSecret;
	}
	
	public void setIsSsl(boolean isSsl){
		this.isSsl = isSsl;
	}
	
	public String getIndex(){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("",np,"GET");
	}
	
	public String getOrders(List<NameValuePair> params){
		return makeApiCall("orders", params, "GET");
	}
	
	public String getOrder(int orderId){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("orders/"+orderId, np, "GET");
	}
	
	public String getOrderCount(){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("orders/count", np, "GET");
	}
	
	public String getOrderNotes(int orderId){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("orders/"+orderId+"/notes", np, "GET");
	}
	
	public String getCoupons(List<NameValuePair> params){
		
		return makeApiCall("coupons", params, "GET");
	}
	
	public String getCoupon(int couponId){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("coupons/"+couponId, np, "GET");
	}
	
	public String getCouponCount(){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("coupons/count", np, "GET");
	}
	
	public String getCouponByCode(String couponCode){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("coupons/code/"+couponCode, np, "GET");
	}
	
	public String getCustomers(List<NameValuePair> params){
		return makeApiCall("customers", params, "GET");
	}
	
	public String getCustomer(int customerId){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("customers/"+customerId, np, "GET");
	}
	
	public String getCustomerByEmail(String email){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("customers/email/"+email, np, "GET");
	}
	
	public String getCustomerCount(){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("customers/count", np, "GET");
	}
	
	public String getCustomerOrders(int customerId){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("customers/"+customerId+"/orders", np, "GET");
	}
	
	public ArrayList<Product> getProducts(List<NameValuePair> params){
		
		String json = makeApiCall("products", params, "GET");
		
		ArrayList<Product> productList = new ArrayList<Product>();
		
		try {
			JSONObject jObj = new JSONObject(json);
			
			if(jObj.has("products") && !jObj.isNull("products")){
				
				JSONArray products	= jObj.getJSONArray("products");
				
				for(int i=0;i<products.length();i++){
					JSONObject product = products.getJSONObject(i);
					
					Product p = ParsingJsonObject.parseProduct(product);
					productList.add(p);
				}
				
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return productList;
	}
	
	public String getProduct(int productId){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("products/"+productId, np, "GET");
	}
	
	public int getProductCount(){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		
		String json	= makeApiCall("products/count", np, "GET");
		
		JSONObject productCount;
		int count=0;
		try {
			productCount	= new JSONObject(json);
			count			= productCount.getInt("count");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return count;
	}
	
	public String getProductReviews(int productId){
		List<NameValuePair> np = new ArrayList<NameValuePair>();
		return makeApiCall("products/"+productId+"/reviews", np, "GET");
	}
	
	public String getReports(List<NameValuePair> param){
		return makeApiCall("reports", param, "GET");
	}
	
	public String getSalesReport(List<NameValuePair> param){
		return makeApiCall("reports/sales", param, "GET");
	}
	
	public String getTopSallersReport(List<NameValuePair> param){
		return makeApiCall("reports/sales/top_sellers", param, "GET");
	}
	
	public String generateOauthSignature(List<NameValuePair> paramList,String method,String endPoint){
		String hash = "";
		try {
			
			String baseRequestUri="";
			try {
				baseRequestUri = URLEncoder.encode(apiUrl+endPoint, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			paramList = normalizeParameters(paramList);
			
			Collections.sort(paramList, new Comparator<NameValuePair>() {
	    	    public int compare(NameValuePair result1, NameValuePair result2) {
	    	        return result1.getName().compareTo(result2.getName());
	    	    }
	    	});
			
			String queryString = "";
			
			for(int i=0;i<paramList.size();i++){
				
				NameValuePair np = paramList.get(i);
				String s = np.getName() + "%3D" + np.getValue();
				
				queryString += s;
				
				if(i != paramList.size()-1){
					queryString += "%26";
				}
			}
			
			String stringToSign = method + "&" + baseRequestUri + "&" + queryString;
			
			SecretKeySpec secret_key 	= new SecretKeySpec(consumerSecret.getBytes(), "HmacSHA256");
			Mac sha256_HMAC 			= Mac.getInstance("HmacSHA256");
			
			sha256_HMAC.init(secret_key);
			hash = Base64.encodeToString(sha256_HMAC.doFinal(stringToSign.getBytes()),Base64.NO_WRAP);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return hash;
	}
	
	private List<NameValuePair> normalizeParameters(List<NameValuePair> paramList){
		
		List<NameValuePair> normalizeParams = new ArrayList<NameValuePair>();
		
		for(int i=0;i<paramList.size();i++){
			NameValuePair np = paramList.get(i);
			
			String key="",value="";
			try {
				key 	= URLEncoder.encode(URLDecoder.decode(np.getName(),"UTF-8"),"UTF-8").replace("%", "%25");
				value	= URLEncoder.encode(URLDecoder.decode(np.getValue(),"UTF-8"),"UTF-8").replace("%", "%25");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			normalizeParams.add(new BasicNameValuePair(key,value));
		}
		
		return normalizeParams;
		
	}
	
	/*private String convertToRFC3986(String str){
		
		str.replace("!", "%21").replace("#", "%23").replace("$", "%24").replace("&", "%26").replace("'", "%27").replace("(", "%28");
		str.replace(")", "%29").replace("*", "%2A").replace("+", "%2B").replace(",", "%2C").replace("/", "%2F").replace(":", "%3A");
		str.replace(";", "%3B").replace("=", "%3D").replace("?", "%3F").replace("@", "%40").replace("[", "%5B").replace("]", "%5D");
		
		return str;
	}*/
	
	public String makeApiCall(String endPoint,List<NameValuePair> paramList,final String method){
		
		String urlStr = "";
		
		String paramStr = "";
		
		if(isSsl){
			urlStr = apiUrl + endPoint;
		}else{
			
			int time 		= (int) (System.currentTimeMillis()/1000);
			long microtime	= System.nanoTime()*1000;
			
			paramList.add(new BasicNameValuePair("oauth_consumer_key", consumerKey));
			paramList.add(new BasicNameValuePair("oauth_timestamp", time+""));
			
			try {
				paramList.add(new BasicNameValuePair("oauth_nonce", AeSimpleSHA1.SHA1(microtime+"")));
			} catch (NoSuchAlgorithmException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			paramList.add(new BasicNameValuePair("oauth_signature_method","HMAC-"+HASH_ALGORITHM));
			
			String signature = generateOauthSignature(paramList, method, endPoint);
			
			paramList.add(new BasicNameValuePair("oauth_signature",signature));
			
			for(int i=0;i<paramList.size();i++){
				
				NameValuePair np = paramList.get(i);
				
				try {
					paramStr += URLEncoder.encode(np.getName(),"utf-8") + "=" + URLEncoder.encode(np.getValue(),"utf-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(i != paramList.size()-1){
					paramStr += "&";
				}
				
			}
			
			urlStr = apiUrl + endPoint +"?" + paramStr;
			mainUrl = urlStr;
			
			//Log.i("url",urlStr);
		}
		HttpURLConnection con	= getHttpConnection(mainUrl, "GET");
		String jsonData = "";
		try {
			jsonData = new GetData(con).execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonData;
	}
	
	
	class GetData extends AsyncTask<Void, Void, String>{
		
		InputStream is;
		String json="";
		JSONObject jObj;
		HttpURLConnection con = null;
		
		public GetData(HttpURLConnection con) {
			// TODO Auto-generated constructor stub
			this.con	= con;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog.show();
		}
		
		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			if(isSsl){
				try {
					urlConnection = (HttpURLConnection) url1.openConnection();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				urlConnection.addRequestProperty(consumerKey, consumerSecret);
			}else{
				con	= getHttpConnection(mainUrl, "GET");
				
				try {
					con.connect();
					
					Log.e("error code",con.getResponseCode() + " : " + con.getResponseMessage());
					
					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			        String temp = null;
			        StringBuilder sb = new StringBuilder();
			        while((temp = in.readLine()) != null){
			            sb.append(temp).append(" ");
			        }
			        json = sb.toString();
			        in.close();
			    } catch (IOException e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			    }
				
				
				/*HttpUriRequest request = new HttpGet(mainUrl);
				StringBuilder builder=new StringBuilder();
				String outhStr = consumerKey + ":" + consumerSecret;
				String base64EncodedCredentials = Base64.encodeToString(outhStr.getBytes(), Base64.NO_WRAP);	
				
				request.addHeader("Authorization", "Basic " + base64EncodedCredentials);
				
				HttpClient httpclient = new DefaultHttpClient();
				
				try {
					HttpResponse response = httpclient.execute(request);
					
					HttpEntity httpentity=response.getEntity();
					is=httpentity.getContent();
					
					BufferedReader br=new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
					
					String line=null;
					while((line=br.readLine())!=null)
					{
						builder.append(line+"\n");
					}
					is.close();
					json=builder.toString();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
			return json;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
		}
	}
	
	public  HttpURLConnection getHttpConnection(String url, String type){
        URL uri = null;
        HttpURLConnection con = null;
        try{
            uri = new URL(url);
            con = (HttpURLConnection) uri.openConnection();
            con.setRequestMethod(type); //type: POST, PUT, DELETE, GET
            /*String outhStr = consumerKey + ":" + consumerSecret;
			String base64EncodedCredentials = Base64.encodeToString(outhStr.getBytes(), Base64.URL_SAFE);	
			con.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);*/
			//con.setDoOutput(true);
            con.setDoInput(true);
            
        }catch(Exception e){
           
        }


        return con;
}
	
}
