package com.example.woocommercedemo.utils;

import java.io.Serializable;

public class ProductDownload implements Serializable{
	
	private static final long serialVersionUID = 162793402766878746L;
	String downloadServerId;
	String fileName;
	String fileUrl;
	
	public ProductDownload() {
		// TODO Auto-generated constructor stub
	}
	
	public ProductDownload(String id,String name,String url){
		downloadServerId	= id;
		fileName			= name;
		fileUrl				= url;
	}
	
	public void setDownloadServerId(String id){
		downloadServerId	= id;
	}
	
	public void setFileName(String fileName){
		this.fileName		= fileName;
	}
	
	public void setFileUrl(String fileUrl){
		this.fileUrl		= fileUrl;
	}
	
	public String getDownloadServerId(){
		return downloadServerId;
	}
	
	public String getFileName(){
		return fileName;
	}
	
	public String getFileUrl(){
		return fileUrl;
	}
}
