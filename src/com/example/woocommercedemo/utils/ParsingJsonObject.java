package com.example.woocommercedemo.utils;

import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParsingJsonObject {
	
	public static Product parseProduct(JSONObject productData){
		
		Product p = new Product();
		String feturedSrc = "";
		
		try {
			
			if(productData.has("id") && !productData.isNull("id")){
				p.setProductServerId(productData.getInt("id"));
			}
			if(productData.has("title") && !productData.isNull("title")){
				p.setProductTtile(productData.getString("title"));
			}
			if(productData.has("price") && !productData.isNull("price")){
				p.setProductPrice(productData.getDouble("price"));
			}
			if(productData.has("description") && !productData.isNull("description")){
				p.setProductDescription(productData.getString("description"));
			}
			if(productData.has("stock_quantity") && !productData.isNull("stock_quantity")){
				p.setStockQuantity(productData.getInt("stock_quantity"));
			}
			if(productData.has("featured_src") && !productData.isNull("featured_src")){
				feturedSrc	= productData.getString("featured_src").replace("\"", "");
				p.setFetureImgSrc(feturedSrc);
			}
			if(productData.has("images") && !productData.isNull("images")){
				
					JSONArray images					= productData.getJSONArray("images");
					
					ArrayList<ImageDetail> galleryImgs	= parseImageDetail(images);
					
					p.setProductImages(galleryImgs);
				
			}
			
			if(productData.has("attributes") && !productData.isNull("attributes")){
				
				ArrayList<ProductAttributes> productAttributes = new ArrayList<ProductAttributes>();
				
				JSONArray attributesArray	= productData.getJSONArray("attributes");
				for(int j=0;j<attributesArray.length();j++){
					JSONObject attribute 	= attributesArray.getJSONObject(j);
					
					ProductAttributes pa		= new ProductAttributes();
					if(attribute.has("name") && !attribute.isNull("name")){
						pa.setAttributeName(attribute.getString("name"));
					}
					if(attribute.has("position") && !attribute.isNull("position")){
						pa.setPosition(attribute.getInt("position"));
					}
					if(attribute.has("visible") && !attribute.isNull("visible")){
						pa.setVisible(attribute.getBoolean("visible"));
					}
					if(attribute.has("variation") && !attribute.isNull("variation")){
						pa.setVariation(attribute.getBoolean("variation"));
					}
					if(attribute.has("options") && !attribute.isNull("options")){
						JSONArray optionArr	= attribute.getJSONArray("options");
						String[] options	= new String[optionArr.length()];
						for(int k=0;k<optionArr.length();k++){
							options[k]		= optionArr.getString(k);
						}
						pa.setOptions(options);
					}
					
					productAttributes.add(pa);
				}
				p.setProductAttributes(productAttributes);
			}
			if(productData.has("variations") && !productData.isNull("variations")){
				JSONArray variationArr	= productData.getJSONArray("variations");
				
				ArrayList<ProductVariation> pvArr	= new ArrayList<ProductVariation>();
				
				for(int x=0;x<variationArr.length();x++){
					pvArr.add(parseProductVariation(variationArr.getJSONObject(x)));
				}
				p.setProductVariations(pvArr);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return p;
	}
	
	
	public static ProductVariation parseProductVariation(JSONObject variation){
		
		ProductVariation prodVariation = new ProductVariation();
		
		try {
			if(variation.has("id") && !variation.isNull("id")){
				prodVariation.setVariationId(variation.getInt("id"));
			}
			if(variation.has("downloadable") && !variation.isNull("downloadable")){
				prodVariation.setVariationDownloadable(variation.getBoolean("downloadable"));
			}
			if(variation.has("virtual") && !variation.isNull("virtual")){
				prodVariation.setVirtual(variation.getBoolean("virtual"));
			}
			if(variation.has("sku") && !variation.isNull("sku")){
				prodVariation.setVariationStockKeepingUnit(variation.getString("sku"));
			}
			if(variation.has("price") && !variation.isNull("price")){
				prodVariation.setPrice(variation.getDouble("price"));
			}
			if(variation.has("regular_price") && !variation.isNull("regular_price")){
				prodVariation.setRegularPrice(variation.getDouble("regular_price"));
			}
			if(variation.has("sale_price") && !variation.isNull("sale_price")){
				prodVariation.setSalePrice(variation.getDouble("sale_price"));
			}
			if(variation.has("stock_quantity") && !variation.isNull("stock_quantity")){
				prodVariation.setStockQuantity(variation.getInt("stock_quantity"));
			}
			if(variation.has("in_stock") && !variation.isNull("in_stock")){
				prodVariation.setInstock(variation.getBoolean("in_stock"));
			}
			if(variation.has("backordered") && !variation.isNull("backordered")){
				prodVariation.setBackOrdered(variation.getBoolean("backordered"));
			}
			if(variation.has("purchaseable") && !variation.isNull("purchaseable")){
				prodVariation.setPurchaseable(variation.getBoolean("purchaseable"));
			}
			if(variation.has("visible") && !variation.isNull("visible")){
				prodVariation.setVisible(variation.getBoolean("visible"));
			}
			if(variation.has("on_sale") && !variation.isNull("on_sale")){
				prodVariation.setOnSale(variation.getBoolean("on_sale"));
			}
			if(variation.has("dimensions") && !variation.isNull("dimensions")){
				JSONObject dimentionObj	= variation.getJSONObject("dimensions");
				ProductDimensions dimention = new ProductDimensions();
				
				if(dimentionObj.has("length") && !dimentionObj.isNull("length")){
					String length	= dimentionObj.getString("length");
					if(!length.equalsIgnoreCase("")){
						dimention.setProductLength(Integer.parseInt(length));
					}
				}
				if(dimentionObj.has("width") && !dimentionObj.isNull("width")){
					String width	= dimentionObj.getString("width");
					if(!width.equalsIgnoreCase("")){
						dimention.setProductWidth(Integer.parseInt(width));
					}
				}
				if(dimentionObj.has("height") && !dimentionObj.isNull("height")){
					String height	= dimentionObj.getString("height");
					if(!height.equalsIgnoreCase("")){
						dimention.setProductHeight(Integer.parseInt(dimentionObj.getString("height")));
					}
				}
				if(dimentionObj.has("unit") && !dimentionObj.isNull("unit")){
					dimention.setUnit(dimentionObj.getString("unit"));
				}
				prodVariation.setVariationDimensions(dimention);
			}
			if(variation.has("image") && !variation.isNull("image")){
				JSONArray variationImgs	= variation.getJSONArray("image");
				ArrayList<ImageDetail> variationImages	= parseImageDetail(variationImgs);
				prodVariation.setVariationProductImages(variationImages.get(0));
			}
			if(variation.has("attributes") && !variation.isNull("attributes")){
				JSONArray variationAttrArr		= variation.getJSONArray("attributes");
				
				Hashtable<String, String> attr	= new Hashtable<String, String>();
				
				for(int k=0;k<variationAttrArr.length();k++){
					
					JSONObject attrObj	= variationAttrArr.getJSONObject(k);
					
					attr.put(attrObj.getString("name"), attrObj.getString("option"));
				}
				prodVariation.setVariationAttributes(attr);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return prodVariation;
	}
	
	public static ArrayList<ImageDetail> parseImageDetail(JSONArray images){
		
		ArrayList<ImageDetail> galleryImgs	= new ArrayList<ImageDetail>();
		
		try{
			for(int j=0;j<images.length();j++){
				
				JSONObject img	= images.getJSONObject(j);
				
				ImageDetail imgDetail = new ImageDetail();
				
				if(img.has("id") && !img.isNull("id")){
					imgDetail.setImageServerId(img.getInt("id"));
				}
				if(img.has("title") && !img.isNull("title")){
					imgDetail.setImageTitle(img.getString("title"));
				}
				if(img.has("position") && !img.isNull("position")){
					imgDetail.setPosition(img.getInt("position"));
				}
				if(img.has("src") && !img.isNull("src")){
					String src		= img.getString("src");
					src				= src.replace("\"", "");
					imgDetail.setImgSrc(src);
				}
				galleryImgs.add(imgDetail);
			}
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return galleryImgs;
		
	}
}
