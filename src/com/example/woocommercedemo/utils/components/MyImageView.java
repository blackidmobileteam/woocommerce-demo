package com.example.woocommercedemo.utils.components;

import android.content.Context;
import android.widget.ImageView;

public class MyImageView extends ImageView{
	
	int position;
	
	public MyImageView(Context context,int position) {
		super(context);
		// TODO Auto-generated constructor stub
		this.position	= position;
	}
	
	public int getImgPosition(){
		return position;
	}

}
