package com.example.woocommercedemo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.SparseArray;
import android.view.View;
import android.widget.ProgressBar;

public class FetchFetureImage extends AsyncTask<Void, Void, SparseArray<Bitmap>>{
	
	Context context;
	ProgressDialog dialog;
	SparseArray<String> imgSrcData;
	SparseArray<Bitmap> images;
	ProgressBar loading;
	
	
	public FetchFetureImage(Context con,SparseArray<String> srcs) {
		// TODO Auto-generated constructor stub
		context	= con;
		dialog	= new ProgressDialog(context);
		dialog.setMessage("Loading Images.........");
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		
		imgSrcData	= srcs;
		images		= new SparseArray<Bitmap>();
	}
	
	public FetchFetureImage(Context con,SparseArray<String> srcs,ProgressBar p) {
		// TODO Auto-generated constructor stub
		context	= con;
		dialog	= new ProgressDialog(context);
		dialog.setMessage("Loading Images.........");
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		
		imgSrcData		= srcs;
		images			= new SparseArray<Bitmap>();
		this.loading	= p;
	}
	
	public void setProgessBar(ProgressBar loading){
		this.loading = loading;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if(loading != null){
			loading.setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected SparseArray<Bitmap> doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		try {
			
			for(int i=0;i<imgSrcData.size();i++){
				
				if(!imgSrcData.get(imgSrcData.keyAt(i)).equalsIgnoreCase("false")){
					URL url	= new URL(imgSrcData.get(imgSrcData.keyAt(i)).toString());
					InputStream input = url.openStream();
					Bitmap bmp = BitmapFactory.decodeStream(input);
					images.put(imgSrcData.keyAt(i), bmp);
			        input.close();
				}
				
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return images;
	}

	@Override
	protected void onPostExecute(SparseArray<Bitmap> result) {
		// TODO Auto-generated method stub 
		super.onPostExecute(result);
		if(loading != null){
			loading.setVisibility(View.GONE);
		}
	}

	
}
