package com.example.woocommercedemo.utils;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable{
	
	private static final long serialVersionUID = 7359857329974837917L;
	int productServerId;
	String productTtile,type,status;
	boolean isProductDownloadable,isProductVirtual,isProductTaxable;
	String stockKeepingUnit;
	double productPrice,productRegularPrice,productSalePrice;
	String productPriceHtml;
	String taxStatus,taxClass;
	boolean managingStock,isProductInStock;
	int stockQuantity;
	boolean isBackOrdersAllowed,isProductBackOrdered,isProductSoldIndividually,isProductPurchaseable,isProductFeatured,isProductVisible;
	String catalogVisibility;
	boolean isProductOnSale;
	String weight;
	ProductDimensions productDimensions;
	boolean isShippingRequired;
	boolean isShippingTaxable;
	String shippingClass;
	String shippingClassId;
	String productDescription;
	String productShortDescription;
	boolean isReviewsAllowed;
	double averageRating;
	int ratingCount;
	int[] relatedIds,upSellIds,crossSellIds;
	ArrayList<String> productCategories;
	ArrayList<ImageDetail> productImages;
	String fetureImgSrc;
	ArrayList<ProductDownload> productDownloads;
	int downloadLimit,downloadExpiryInDays,downloadType,totalSales;
	String purchaseNote;
	ArrayList<ProductAttributes> productAttributes;
	ArrayList<ProductVariation> productVariations;
	
	public Product(){
		productVariations	= new ArrayList<ProductVariation>();
	}

	public int getProductServerId() {
		return productServerId;
	}


	public void setProductServerId(int productServerId) {
		this.productServerId = productServerId;
	}


	public String getProductTtile() {
		return productTtile;
	}


	public void setProductTtile(String productTtile) {
		this.productTtile = productTtile;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public boolean isProductDownloadable() {
		return isProductDownloadable;
	}


	public void setProductDownloadable(boolean isProductDownloadable) {
		this.isProductDownloadable = isProductDownloadable;
	}


	public boolean isProductVirtual() {
		return isProductVirtual;
	}


	public void setProductVirtual(boolean isProductVirtual) {
		this.isProductVirtual = isProductVirtual;
	}


	public boolean isProductTaxable() {
		return isProductTaxable;
	}


	public void setProductTaxable(boolean isProductTaxable) {
		this.isProductTaxable = isProductTaxable;
	}


	public String getStockKeepingUnit() {
		return stockKeepingUnit;
	}


	public void setStockKeepingUnit(String stockKeepingUnit) {
		this.stockKeepingUnit = stockKeepingUnit;
	}


	public double getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}


	public double getProductRegularPrice() {
		return productRegularPrice;
	}


	public void setProductRegularPrice(double productRegularPrice) {
		this.productRegularPrice = productRegularPrice;
	}


	public double getProductSalePrice() {
		return productSalePrice;
	}


	public void setProductSalePrice(double productSalePrice) {
		this.productSalePrice = productSalePrice;
	}


	public String getProductPriceHtml() {
		return productPriceHtml;
	}


	public void setProductPriceHtml(String productPriceHtml) {
		this.productPriceHtml = productPriceHtml;
	}


	public String getTaxStatus() {
		return taxStatus;
	}


	public void setTaxStatus(String taxStatus) {
		this.taxStatus = taxStatus;
	}


	public String getTaxClass() {
		return taxClass;
	}


	public void setTaxClass(String taxClass) {
		this.taxClass = taxClass;
	}


	public boolean isManagingStock() {
		return managingStock;
	}


	public void setManagingStock(boolean managingStock) {
		this.managingStock = managingStock;
	}


	public boolean isProductInStock() {
		return isProductInStock;
	}


	public void setProductInStock(boolean isProductInStock) {
		this.isProductInStock = isProductInStock;
	}


	public int getStockQuantity() {
		return stockQuantity;
	}


	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}


	public boolean isBackOrdersAllowed() {
		return isBackOrdersAllowed;
	}


	public void setBackOrdersAllowed(boolean isBackOrdersAllowed) {
		this.isBackOrdersAllowed = isBackOrdersAllowed;
	}


	public boolean isProductBackOrdered() {
		return isProductBackOrdered;
	}


	public void setProductBackOrdered(boolean isProductBackOrdered) {
		this.isProductBackOrdered = isProductBackOrdered;
	}


	public boolean isProductSoldIndividually() {
		return isProductSoldIndividually;
	}


	public void setProductSoldIndividually(boolean isProductSoldIndividually) {
		this.isProductSoldIndividually = isProductSoldIndividually;
	}


	public boolean isProductPurchaseable() {
		return isProductPurchaseable;
	}


	public void setProductPurchaseable(boolean isProductPurchaseable) {
		this.isProductPurchaseable = isProductPurchaseable;
	}


	public boolean isProductFeatured() {
		return isProductFeatured;
	}


	public void setProductFeatured(boolean isProductFeatured) {
		this.isProductFeatured = isProductFeatured;
	}


	public boolean isProductVisible() {
		return isProductVisible;
	}


	public void setProductVisible(boolean isProductVisible) {
		this.isProductVisible = isProductVisible;
	}


	public String getCatalogVisibility() {
		return catalogVisibility;
	}


	public void setCatalogVisibility(String catalogVisibility) {
		this.catalogVisibility = catalogVisibility;
	}


	public boolean isProductOnSale() {
		return isProductOnSale;
	}


	public void setProductOnSale(boolean isProductOnSale) {
		this.isProductOnSale = isProductOnSale;
	}


	public String getWeight() {
		return weight;
	}


	public void setWeight(String weight) {
		this.weight = weight;
	}


	public ProductDimensions getProductDimensions() {
		return productDimensions;
	}


	public void setProductDimensions(ProductDimensions productDimensions) {
		this.productDimensions = productDimensions;
	}


	public boolean isShippingRequired() {
		return isShippingRequired;
	}


	public void setShippingRequired(boolean isShippingRequired) {
		this.isShippingRequired = isShippingRequired;
	}


	public boolean isShippingTaxable() {
		return isShippingTaxable;
	}


	public void setShippingTaxable(boolean isShippingTaxable) {
		this.isShippingTaxable = isShippingTaxable;
	}


	public String getShippingClass() {
		return shippingClass;
	}


	public void setShippingClass(String shippingClass) {
		this.shippingClass = shippingClass;
	}


	public String getShippingClassId() {
		return shippingClassId;
	}


	public void setShippingClassId(String shippingClassId) {
		this.shippingClassId = shippingClassId;
	}


	public String getProductDescription() {
		return productDescription;
	}


	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}


	public String getProductShortDescription() {
		return productShortDescription;
	}


	public void setProductShortDescription(String productShortDescription) {
		this.productShortDescription = productShortDescription;
	}


	public boolean isReviewsAllowed() {
		return isReviewsAllowed;
	}


	public void setReviewsAllowed(boolean isReviewsAllowed) {
		this.isReviewsAllowed = isReviewsAllowed;
	}


	public double getAverageRating() {
		return averageRating;
	}


	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}


	public int getRatingCount() {
		return ratingCount;
	}


	public void setRatingCount(int ratingCount) {
		this.ratingCount = ratingCount;
	}


	public int[] getRelatedIds() {
		return relatedIds;
	}


	public void setRelatedIds(int[] relatedIds) {
		this.relatedIds = relatedIds;
	}


	public int[] getUpSellIds() {
		return upSellIds;
	}


	public void setUpSellIds(int[] upSellIds) {
		this.upSellIds = upSellIds;
	}


	public int[] getCrossSellIds() {
		return crossSellIds;
	}


	public void setCrossSellIds(int[] crossSellIds) {
		this.crossSellIds = crossSellIds;
	}


	public ArrayList<String> getProductCategories() {
		return productCategories;
	}


	public void setProductCategories(ArrayList<String> productCategories) {
		this.productCategories = productCategories;
	}


	public ArrayList<ImageDetail> getProductImages() {
		return productImages;
	}


	public void setProductImages(ArrayList<ImageDetail> productImages) {
		this.productImages = productImages;
	}


	public String getFetureImgSrc() {
		return fetureImgSrc;
	}


	public void setFetureImgSrc(String fetureImgSrc) {
		this.fetureImgSrc = fetureImgSrc;
	}


	public ArrayList<ProductDownload> getProductDownloads() {
		return productDownloads;
	}


	public void setProductDownloads(ArrayList<ProductDownload> productDownloads) {
		this.productDownloads = productDownloads;
	}


	public int getDownloadLimit() {
		return downloadLimit;
	}


	public void setDownloadLimit(int downloadLimit) {
		this.downloadLimit = downloadLimit;
	}


	public int getDownloadExpiryInDays() {
		return downloadExpiryInDays;
	}


	public void setDownloadExpiryInDays(int downloadExpiryInDays) {
		this.downloadExpiryInDays = downloadExpiryInDays;
	}


	public int getDownloadType() {
		return downloadType;
	}


	public void setDownloadType(int downloadType) {
		this.downloadType = downloadType;
	}


	public int getTotalSales() {
		return totalSales;
	}


	public void setTotalSales(int totalSales) {
		this.totalSales = totalSales;
	}


	public String getPurchaseNote() {
		return purchaseNote;
	}


	public void setPurchaseNote(String purchaseNote) {
		this.purchaseNote = purchaseNote;
	}

	public ArrayList<ProductAttributes> getProductAttributes() {
		return productAttributes;
	}

	public void setProductAttributes(ArrayList<ProductAttributes> productAttributes) {
		this.productAttributes = productAttributes;
	}

	public ArrayList<ProductVariation> getProductVariations() {
		return productVariations;
	}

	public void setProductVariations(ArrayList<ProductVariation> productVariations) {
		this.productVariations = productVariations;
	}
	
}

