package com.example.woocommercedemo.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

public class ProductVariation implements Serializable{
	
	private static final long serialVersionUID = 3825760135700374300L;
	
	int variationId;
	boolean isVariationDownloadable;
	boolean isVirtual;
	String variationStockKeepingUnit;
	double price,regularPrice,salePrice;
	int stockQuantity;
	boolean isInstock,isBackOrdered,isPurchaseable,isOnSale,isVisible;
	ProductDimensions variationDimensions;
	ImageDetail variationProductImages;
	Hashtable<String, String> variationAttributes;
	
	public ProductVariation() {
		// TODO Auto-generated constructor stub
	}
	
	public int getVariationId() {
		return variationId;
	}
	public void setVariationId(int variationId) {
		this.variationId = variationId;
	}
	public boolean isVariationDownloadable() {
		return isVariationDownloadable;
	}
	public void setVariationDownloadable(boolean isVariationDownloadable) {
		this.isVariationDownloadable = isVariationDownloadable;
	}
	public String getVariationStockKeepingUnit() {
		return variationStockKeepingUnit;
	}
	public void setVariationStockKeepingUnit(String variationStockKeepingUnit) {
		this.variationStockKeepingUnit = variationStockKeepingUnit;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getRegularPrice() {
		return regularPrice;
	}
	public void setRegularPrice(double regularPrice) {
		this.regularPrice = regularPrice;
	}
	public double getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}
	public int getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public boolean isInstock() {
		return isInstock;
	}
	public void setInstock(boolean isInstock) {
		this.isInstock = isInstock;
	}
	public boolean isBackOrdered() {
		return isBackOrdered;
	}
	public void setBackOrdered(boolean isBackOrdered) {
		this.isBackOrdered = isBackOrdered;
	}
	public boolean isPurchaseable() {
		return isPurchaseable;
	}
	public void setPurchaseable(boolean isPurchaseable) {
		this.isPurchaseable = isPurchaseable;
	}
	public boolean isOnSale() {
		return isOnSale;
	}
	public void setOnSale(boolean isOnSale) {
		this.isOnSale = isOnSale;
	}
	public ProductDimensions getVariationDimensions() {
		return variationDimensions;
	}
	public void setVariationDimensions(ProductDimensions variationDimensions) {
		this.variationDimensions = variationDimensions;
	}
	
	public ImageDetail getVariationProductImages() {
		return variationProductImages;
	}

	public void setVariationProductImages(ImageDetail variationProductImages) {
		this.variationProductImages = variationProductImages;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Hashtable<String, String> getVariationAttributes() {
		return variationAttributes;
	}

	public void setVariationAttributes(Hashtable<String, String> variationAttributes) {
		this.variationAttributes = variationAttributes;
	}

	public boolean isVirtual() {
		return isVirtual;
	}

	public void setVirtual(boolean isVirtual) {
		this.isVirtual = isVirtual;
	}
	
	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

}
