package com.example.woocommercedemo;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;

import com.example.woocommercedemo.utils.ImageDetail;
import com.example.woocommercedemo.utils.Product;
import com.example.woocommercedemo.utils.ProductAttributes;
import com.example.woocommercedemo.utils.ProductVariation;
import com.example.woocommercedemo.utils.adapter.VariationSpinnerAdapter;
import com.example.woocommercedemo.utils.components.MyImageView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

public class ProductDetailActivity extends Activity{
	
	LinearLayout galleryImages;
	TextView productTitle,productPrice;
	WebView productDescription;
	ImageView fetureImg;
	EditText quantity;
	ProgressBar fetureProgress,galleryProgress;
	Spinner variation,variationType;
	
	Product product;
	ArrayList<ImageDetail> galleryImagesData;
	ArrayList<ProductAttributes> productAttributes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_detail);
		
		Intent i			= getIntent();
		product				= (Product) i.getSerializableExtra("product");
		
		productTitle		= (TextView)findViewById(R.id.product_title);
		productDescription	= (WebView)findViewById(R.id.product_description);
		productPrice		= (TextView)findViewById(R.id.product_price);
		fetureImg			= (ImageView)findViewById(R.id.feture_img);
		galleryImages		= (LinearLayout)findViewById(R.id.gallery_img);
		quantity			= (EditText)findViewById(R.id.quantity);
		fetureProgress		= (ProgressBar)findViewById(R.id.feture_img_loading);
		galleryProgress		= (ProgressBar)findViewById(R.id.gallery_loading);
		variation			= (Spinner)findViewById(R.id.variation);
		variationType		= (Spinner)findViewById(R.id.variation_type);
		
		productTitle.setText(product.getProductTtile());
		productDescription.loadData(product.getProductDescription(), "text/html", "utf-8");
		productPrice.setText(product.getProductPrice()+"");
		
		productAttributes	= product.getProductAttributes();
		
		VariationSpinnerAdapter spinnerAdapter	= new VariationSpinnerAdapter(ProductDetailActivity.this, android.R.layout.simple_spinner_item, productAttributes);
		variation.setAdapter(spinnerAdapter);
		
		variation.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				// TODO Auto-generated method stub
				//Log.i("options","option size : "+productAttributes.get(position).getOptions().length);
				ArrayAdapter<String> variationTypeAdapter	= new ArrayAdapter<String>(ProductDetailActivity.this, android.R.layout.simple_spinner_item, productAttributes.get(position).getOptions());
				variationTypeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
				variationType.setAdapter(variationTypeAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		variationType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				ProductAttributes sp	= (ProductAttributes) variation.getSelectedItem();
				String op = sp.getOptions()[position];
				Log.e("option",op);
				ProductVariation pv = new ProductVariation();
				
				ArrayList<ProductVariation> variationList = product.getProductVariations();
				
				for(int i=0;i<variationList.size();i++){
					Hashtable<String, String> option	= variationList.get(i).getVariationAttributes();
					Log.e("option size",""+option.size());
					if(option.contains(op.toLowerCase())){
						pv = variationList.get(i);
						productPrice.setText(pv.getPrice()+"");
						Log.e("price",pv.getPrice()+"");
						Log.e("img_url",pv.getVariationProductImages().getImgSrc()+"");
						new FetchVariationImage(pv.getVariationProductImages().getImgSrc()).execute();
						break;
					}
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		galleryImagesData	= product.getProductImages();
		
		new FetchProductImages().execute();
		
	}
	
	class FetchVariationImage extends AsyncTask<Void, Void, Void>{
		
		String urlStr;
		Bitmap bmp;
		
		public FetchVariationImage(String url) {
			// TODO Auto-generated constructor stub
			this.urlStr	= url;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			fetureProgress.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				if(!urlStr.equalsIgnoreCase("false")){
					URL url	= new URL(urlStr);
					InputStream input = url.openStream();
					bmp = BitmapFactory.decodeStream(input);
					input.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			fetureProgress.setVisibility(View.GONE);
			fetureImg.setImageBitmap(bmp);
		}
		
	}
	
	class FetchProductImages extends AsyncTask<Void, Void, Void>{
		
		ArrayList<Bitmap> imagesBitmap;
		int fetureImgIndex;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			fetureProgress.setVisibility(View.VISIBLE);
			galleryProgress.setVisibility(View.VISIBLE);
			
			imagesBitmap	= new ArrayList<Bitmap>();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String fetureSrc	= product.getFetureImgSrc();
			
			try {
				for(int i=0;i<galleryImagesData.size();i++){
					
					String src	= galleryImagesData.get(i).getImgSrc();
					if(!src.equalsIgnoreCase("false")){
						URL url	= new URL(src);
						InputStream input = url.openStream();
						Bitmap bmp = BitmapFactory.decodeStream(input);
						imagesBitmap.add(bmp);
						if(src.equals(fetureSrc)){
							fetureImgIndex = imagesBitmap.size()-1;
						}
				        input.close();
					}
					
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			fetureProgress.setVisibility(View.GONE);
			galleryProgress.setVisibility(View.GONE);
			//Log.i("gallery size",""+imagesBitmap.size());
			for(int i=0;i<imagesBitmap.size();i++){
				MyImageView img	= new MyImageView(ProductDetailActivity.this, i);
				img.setImageBitmap(imagesBitmap.get(i));
				LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(80, 80);
				layoutParams.setMargins(0, 0, 5, 0);
				galleryImages.addView(img);
			}
			
			fetureImg.setImageBitmap(imagesBitmap.get(fetureImgIndex));
			
		}

	}
}
