package com.example.woocommercedemo;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.widget.ProgressBar;

public class MainActivity extends Activity {
	
	private int progressBarStatus = 0;


	private Handler progressBarHandler = new Handler();
	ProgressBar progress_bar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		progressBarStatus = 0;
		
		progress_bar = (ProgressBar) findViewById(R.id.progressBar1);
		
		Drawable customDrawable= getResources().getDrawable(R.drawable.custom_progressbar);
		progress_bar.setProgressDrawable(customDrawable); 
		progress_bar.setProgress(0);
		
		new Thread(new Runnable() {
			public void run() {
				while (progressBarStatus < 100) {

					// process some tasks
					progressBarStatus++;

					// your computer is too fast, sleep 1 second
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Update the progress bar
					progressBarHandler.post(new Runnable() {
						public void run() {
							progress_bar.setProgress(progressBarStatus);
						}
					});
				}
				
				
				
				// ok, file is downloaded,
				if (progressBarStatus >= 20) {

					
					Intent mainIntent = new Intent(MainActivity.this,ProductList.class);
					
					MainActivity.this.startActivity(mainIntent);
					MainActivity.this.finish();
					
				}
			}
		}).start();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
