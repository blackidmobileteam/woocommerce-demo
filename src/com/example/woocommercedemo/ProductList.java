package com.example.woocommercedemo;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.example.woocommercedemo.utils.FetchFetureImage;
import com.example.woocommercedemo.utils.Product;
import com.example.woocommercedemo.utils.WcApiClient;
import com.example.woocommercedemo.utils.adapter.ProductListAdapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProductList extends Activity{
	
	public String consumerKey,consumerSecret,storeUrl;
	ArrayList<Product> productListData;
	SparseArray<String> productImgSrc;
	SparseArray<Bitmap> productFetureImgs;
	ProductListAdapter adapter;
	Button prevProducts,nextProducts;
	int perPageSize;
	int index =1;
	int totalProducts;
	int totalPages;
	
	ListView productList;
	ProgressBar imgProgress;
	//EditText searchText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_list);
		
		consumerKey			= "ck_225c44907e65e1ce2dc81d0b77f0d7f7";
		consumerSecret		= "cs_f82ca9edf4d6a2423d3963419cc32e5b";
		storeUrl			= "http://192.168.1.18/wp/wedding-tweets/";
		
		productList			= (ListView)findViewById(R.id.product_list);
		imgProgress			= (ProgressBar)findViewById(R.id.image_progress);
		prevProducts		= (Button)findViewById(R.id.prev_btn);
		nextProducts		= (Button)findViewById(R.id.next_btn);
		//searchText			= (EditText)findViewById(R.id.search_txt);
		
		/*consumerKey		= "ck_29ddc932a28a6edbc76ed89096e86692";
		consumerSecret		= "cs_b4601103595e8f433c128a7282ab87f2";
		storeUrl			= "http://192.168.1.20/vrder/";*/
		
		final WcApiClient wc		= new WcApiClient(consumerKey, consumerSecret, storeUrl,ProductList.this);
		
		ArrayList<NameValuePair> prodctCondition = new ArrayList<NameValuePair>();
		prodctCondition.add(new BasicNameValuePair("page", ""+index));
		
		totalProducts		= wc.getProductCount();
		
		productListData		= wc.getProducts(prodctCondition);
		perPageSize			= productListData.size();
		
		totalPages			= totalProducts/perPageSize;
		
		if(totalProducts%perPageSize > 0){
			totalPages		= totalPages + 1;
		}
		
		Log.i("no of pages",""+totalPages);
		
		adapter 			= new ProductListAdapter(ProductList.this, productListData);
		productList.setAdapter(adapter);
		
		productImgSrc		= new SparseArray<String>();
		
		fetchProductImages();
		
		productList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ProductList.this,ProductDetailActivity.class);
				i.putExtra("product", productListData.get(position));
				startActivity(i);
			}
		});
		
		prevProducts.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(index > 1){
					index = index - 1;
					
					ArrayList<Product> productData 				= new ArrayList<Product>();
					ArrayList<NameValuePair> prodctCondition 	= new ArrayList<NameValuePair>();
					
					prodctCondition.add(new BasicNameValuePair("page", ""+index));
					
					productData		= wc.getProducts(prodctCondition);
					productListData.clear();
					productListData	= productData;
					productFetureImgs.clear();
					adapter 		= new ProductListAdapter(ProductList.this, productListData);
					productList.setAdapter(adapter);
					fetchProductImages();
				}
						
			}
		});
		
		/*searchText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
		nextProducts.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(index<totalPages){
					index++;
					ArrayList<Product> productData 				= new ArrayList<Product>();
					ArrayList<NameValuePair> prodctCondition 	= new ArrayList<NameValuePair>();
					
					prodctCondition.add(new BasicNameValuePair("page", ""+index));
					
					productData		= wc.getProducts(prodctCondition);
					productListData.clear();
					productListData	= productData;
					productFetureImgs.clear();
					adapter 		= new ProductListAdapter(ProductList.this, productListData);
					productList.setAdapter(adapter);
					fetchProductImages();
				}
				
			}
		});
		
	}
	
	public void fetchProductImages(){
		
		for(int i=0;i<productListData.size();i++){
			
			//Log.i("key",productListData.get(i).getProductServerId()+"");
			//Log.i("value",productListData.get(i).getFetureImgSrc());
			
			productImgSrc.put(productListData.get(i).getProductServerId(), productListData.get(i).getFetureImgSrc());
		}
		
		try {
			productFetureImgs	= new FetchFetureImage(ProductList.this, productImgSrc,imgProgress).execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		adapter.setProductImgData(productFetureImgs);
		adapter.notifyDataSetChanged();
	}
}

